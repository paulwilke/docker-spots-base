"1. Load Seaside"
Metacello new
 	baseline:'Seaside3';
	githubUser: 'SeasideSt' project: 'Seaside' commitish: 'v3.4.4' path: 'repository';
	onConflictUseIncoming;
	silently;
	onWarningLog;
	load.

"2. I18N"
Gofer new
    url: 'http://smalltalkhub.com/mc/TorstenBergmann/I18N/main';
    package: 'ConfigurationOfI18N';
    load.
((Smalltalk at: #ConfigurationOfI18N) project stableVersion) load.

"3. Init File from Catalog"
Metacello new
	repository: 'github://astares/Pharo-INIFile/src';
	baseline: 'INIFile';
	load.

"4. NeoJSON"
Metacello new
  repository: 'github://svenvc/NeoJSON/repository';
  baseline: 'NeoJSON';
  load.

"5. Voyage"
Metacello new
	githubUser: 'pharo-nosql' project: 'voyage' commitish: '1.7.1' path: 'mc';
	baseline: 'Voyage';
	onConflictUseIncoming;
	onWarningLog;
    silently;
	load: 'mongo tests'.

"6. TelePharo"
 Metacello new
   baseline: 'TelePharo';
   repository: 'github://pharo-ide/TelePharo';
   load: 'Server'.
