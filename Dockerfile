# Stage 1: Load the project
FROM basmalltalk/pharo:7.0-image AS loader
LABEL maintainer="Paul Wilke <wilke@itsys24.de>"
COPY  ./scripts/load.st ./
RUN pharo Pharo.image load.st --save --quit

# Stage 2: Copy the resulting Pharo.image with our project loaded
# into a new docker image with just the vm
FROM basmalltalk/pharo:7.0
WORKDIR /app
COPY --from=loader /opt/pharo/Pharo.image ./
COPY --from=loader /opt/pharo/Pharo.changes ./
COPY --from=loader /opt/pharo/Pharo*.sources ./
COPY ./scripts/start.st ./

USER root
RUN chown pharo:users /app -R

EXPOSE 8080
EXPOSE 40423

USER pharo
CMD [ "pharo", "Pharo.image", "start.st" ]
